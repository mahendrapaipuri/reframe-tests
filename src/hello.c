// Copyright 2016-2021 Swiss National Supercomputing Centre (CSCS/ETH Zurich)
// ReFrame Project Developers. See the top-level LICENSE file for details.
//
// SPDX-License-Identifier: BSD-3-Clause

#include <stdio.h>


int main(int argc, char* argv[])
{
    printf("Hello, World from %s!\n", argv[1]);
    return 0;
}
