// Copyright 2016-2021 Swiss National Supercomputing Centre (CSCS/ETH Zurich)
// ReFrame Project Developers. See the top-level LICENSE file for details.
//
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>


int main(int argc, char* argv[])
{
    std::cout << "Hello, World from " << argv[1] << "!\n"; 
    return 0;
}
