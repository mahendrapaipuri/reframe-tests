"""ReFrame configuration file."""

import os
import sys


site_configuration = {
    'systems': [
        # < insert new systems here >
        {
            'name': 'generic',
            'descr': 'Generic system. When no system is matched, it falls back to generic system',
            'hostnames': ['.*'],
            'modules_system': 'nomod',
            'partitions': [
                {
                    'name': 'mypartition-1',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['builtin'],
                },
            ]
        },  # end generic
    ],
    'environments': [
        {
            'name': 'gnu',
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran'
        },
        {
            'name': 'clang',
            'cc': 'clang',
            'cxx': 'clang++',
            'ftn': ''
        },
        {
            'name': 'builtin',
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran',
            'target_systems': [
                'generic:mypartition-1',
            ]
        },
    ],
    'logging': [
        {
            'level': 'debug',
            'handlers': [
                {
                    'type': 'file',
                    'name': 'reframe.log',
                    'level': 'debug',
                    'format': '[%(asctime)s] %(levelname)s: %(check_name)s: %(message)s',   # noqa: E501
                    'append': False
                },
                {
                    'type': 'stream',
                    'name': 'stdout',
                    'level': 'info',
                    'format': '%(message)s'
                },
                {
                    'type': 'file',
                    'name': 'reframe.out',
                    'level': 'info',
                    'format': '%(message)s',
                    'append': False
                }
            ],
            'handlers_perflog': [
                {
                    'type': 'filelog',
                    # make this the same as output filenames which are ('sysname', 'partition',
                    # 'environ', 'testname', 'filename')
                    'prefix': '%(check_system)s/%(check_partition)s/%(check_environ)s'
                              '/%(check_name)s', # <testname>.log gets appended
                    'level': 'info',
                    # added units here - see Reference:
                    # https://reframe-hpc.readthedocs.io/en/latest/config_reference.html?highlight=perflog#logging-.handlers_perflog
                    'format': '%(check_job_completion_time)s|reframe %(version)s|%(check_info)s|'
                              'jobid=%(check_jobid)s|%(check_perf_var)s=%(check_perf_value)s|'
                              '%(check_perf_unit)s|ref=%(check_perf_ref)s '
                              '(l=%(check_perf_lower_thres)s, '
                              'u=%(check_perf_upper_thres)s)|%(check_tags)s',  # noqa: E501
                    'datefmt': '%FT%T%:z',
                    'append': True
                }
            ]
        }
    ],
    'general': [
        {
            'check_search_path': ['./'],
            'check_search_recursive': True,
            'purge_environment': True,
            'remote_detect': False,
        }
    ]    
}
