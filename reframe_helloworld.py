
import reframe as rfm
import reframe.utility.sanity as sn


@rfm.simple_test
class HelloMultiLangTest(rfm.RegressionTest):

    lang = parameter(['c', 'cpp'])
    arg = parameter(['Mercury', 'Venus', 'Mars'])

    def __init__(self):

        self.valid_systems = ['*']
        self.valid_prog_environs = ['builtin']
        self.tags |= {self.lang, self.arg}
        self.executable_opts = [self.arg, '> hello.out']
        self.sanity_patterns = sn.assert_found(
            r'Hello, World from {}!'.format(self.arg), 'hello.out')

    @run_before('compile')
    def set_sourcepath(self):
        self.sourcepath = f'hello.{self.lang}'
